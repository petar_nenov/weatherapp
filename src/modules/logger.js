const succes = (message = "Default message") =>
  console.log(`Succes ${message}`);
const error = (message = "Default message") => console.log(`Error ${message}`);

export { succes, error };
