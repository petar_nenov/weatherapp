export const getWeather = async town => {
  const baseDailyURI = "api.openweathermap.org/data/2.5/weather?q=";
  const appid = "8faec031c64bceec12c32c27e7cc7797";
  const selectedTown = town;
  const uri = `https://${baseDailyURI}${selectedTown}&appid=${appid}`;
  const response = await fetch(uri);
  const weatherTownInfo = await response.json();
  return weatherTownInfo;
};
