import { getWeather } from "./getWeather";
import { fillDailyData} from './fillDailyData';

export const townSelector = () => {
  $("#towns").append('<select id="town" ></select>');
  $("#town")
    .append('<optgroup label = "Select one ...">')
    .append('<option value="Sofia">Sofia</option>')
    .append('<option value="Pleven">Pleven</option>')
    .append('<option value="London">London</option>')
    .append('<option value="Timra">Timra</option>');
  $("#select-wrapper").append('<button id="button-getInfo">Get forcast</button>');
  $("#button-getInfo").on("click", event =>{ 
    const selectedTown = $("#town").val();    
    const dailyWeatherData =  getWeather(selectedTown);      
    dailyWeatherData.then(data=>{
      fillDailyData(data);
    }) ;
  });
};
