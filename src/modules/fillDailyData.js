import { kelvinToCelsius } from "./kelvinToCelsius";

const noInfoMessage = "no info";

export const fillDailyData = obj => {
  const {
    clouds = {},
    coord = {},
    main = {},
    weather = {},
    wind = {},
    rain = {}
  } = obj;

  $("#daily-temperature").text(kelvinToCelsius(main.temp) || noInfoMessage);
  $("#daily-weather").text(weather[0].description || noInfoMessage);
  $("#daily-wind").text(wind.speed.toString() || noInfoMessage);
  $("#daily-cloudiness").text(clouds.all.toString() || noInfoMessage);
  $("#daily-pressure").text(main.pressure.toString() || noInfoMessage);
  $("#daily-rain").text(rain["1h"] || noInfoMessage);
  $("#daily-geo").text(`[${coord.lon},${coord.lat}]` || noInfoMessage);
};
