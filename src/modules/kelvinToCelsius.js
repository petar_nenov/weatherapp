export const kelvinToCelsius = kelvins => {
  return (kelvins - 273.15).toFixed(1);
};
