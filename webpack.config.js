//webpack configuration file
// npm install --save-dev webpack webpack-dev-server
// npm i -D @babel/core @babel/preset-env babel-loader
//npm i -D @babel/polyfill @babel/plugin-transformtransform-async-to-generator
const path = require("path");

module.exports = {
  mode: "development",
  entry: ["@babel/polyfill", "./src/app.js"],
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "app.bundle.js",
    publicPath: "/build/"
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          presets: ["@babel/preset-env"],
          plugins: ["@babel/transform-async-to-generator"]
        }
      },
      {
        test: /\.css?$/,
        loader: "style-loader!css-loader"
      }
    ]
  }
};
